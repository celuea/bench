### -*- coding: cp936 -*-
### mysetup.py
##from distutils.core import setup
##import py2exe
##
##setup(console=["stgyRunner.py"])
##


#coding=utf-8

from distutils.core import setup
import py2exe

options = {"py2exe":
  {"compressed": 1, #压缩   
  "optimize": 2,   
  "bundle_files": 1 #所有文件打包成一个exe文件  
  }
}

setup(
  # The first three parameters are not required, if at least a
  # 'version' is given, then a versioninfo resource is built from
  # them and added to the executables.
  version = "1.0.0",
  description = "write by xianwei",
  name = "xxxxxx",
    
  options = options,
  zipfile=None, #不生成library.zip文件
    
  # targets to build
  #console = [{"script": "stgyRunner.py", "icon_resources": [(1, "python.ico")] }]#源文件，程序图标
  #console=["strategyBench.py"]
  console=["strategyBench.py"]
  )
