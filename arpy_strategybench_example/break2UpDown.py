# -*- coding: cp936 -*-
from candle import candle
import logging
import ConfigParser
'''
��������:




'''
class break2UpDown(object):
    confname='stgyPanTupo.ini'
    def __init__(self, symbol,cycle):#
        configFl = ConfigParser.RawConfigParser()
        configFl.read(break2UpDown.confname)
        self.a = configFl.getint("oneStock", "a")
        self.b = configFl.getint("oneStock", "b")
        self.c = configFl.getint("oneStock", "c")
        self.d = configFl.getint("oneStock", "d")
        self.e = configFl.getint("oneStock", "e")
        self.f = configFl.getint("oneStock", "f")
        self.longshort = configFl.getint("oneStock", "longshort")
    
        self.symbol = symbol
        
        self.m = 0.0
        self.t = 0

        self.m1 = 0.0
        self.t1 = 0
        self.m2 = 0.0
        self.t2 = 0

        self.t3 = 0
        self.m3 = 0.0
        self.up = 0
        self.down = 0

        self.upM2Num = 0

        #config
        self.cycle = 500
        self.minPxSpan = 0.1
        self.m_m1_max = 2
        self.cur_t = 180
        self.m3_m_max = 0.5
        self.m3_m2_max = 0.2
        self.m2_m3 = 0.2
        self.t_t1_min = 30
        self.t_t1_max = 60
        self.reset_t = 7200
        self.tVib = 1
        self.deqK = []

##        self.a=0
##        self.b=0
##        self.c=0
##        self.d=0
##        self.e=0
##        self.f=0
##        self.longshort=0
        self.resetup()
        self.resetdown()

    def resetup(self):
        print "enter reset"
        self.up = 0
        
##        self.m3 = 0.0
##        self.upM2Num = 0
##        self.m1 = 0.0
##        self.t1 = 0
##        self.m = 0.0
##        self.t = 0
##        self.m2= 0.0
##        self.t2 = 0
##        self.m3 = 0.0
##
##        
        self.continUp=0

        self.TMax=0.0
        self.TMin=99999.9
        self.tMin=999999.9
    def resetdown(self):
        print "enter reset"
        self.down = 0
##        
##        self.m3 = 0.0
##        self.upM2Num = 0
##        self.m1 = 0.0
##        self.t1 = 0
##        self.m = 0.0
##        self.t = 0
##        self.m2= 0.0
##        self.t2 = 0
##        self.m3 = 0.0

        self.contindown=0

        self.TMaxdown=0.0
        self.TMindown=99999.9
        self.tMindown=999999.9

        #print "out reset"
        
    def checkUp(self,tmpK,count):
        
        lastK = self.deqK[count - 2]

        
        if self.continUp >= 2:
            #print tmpK.high,self.TMax,self.TMin,self.c,self.d,self.continUp,self.up,"--------"
            logging.error("checkUp:continUp="+str(self.continUp)+",high="+str(tmpK.high)+",TMax="+str(self.TMax)+",TMin="+str(self.TMin)+",c="+str(self.c)+",d="+str(self.d))
            if tmpK.high > self.TMax:
                #print self.TMax,self.TMin,self.c,self.d,"============"
                return 1
            else:
                self.resetup()
                #print "checkUp---===++++"
                logging.error("checkUp:resetup=high-TMax")
    
#        tmplog= self.symbol + ",self.down,m3m:" + str(m3m) + ",m3_m_max:" + str(self.m3_m_max)+ ",m3m2:" + str(m3m2) + ",m3_m2_max:" + str(self.m3_m2_max)+ ",m_m1_max:" + str(self.m_m1_max) + ",m:" + str(self.m)+ ",m1:" + str(self.m1)+ ",m2:" + str(self.m2) + ",m3:" + str(self.m3) +",t3:" + str(self.t3) + ",m2_m3:" + str(self.m2_m3)+ ",t:" + str(self.t) + ",t1:" + str(self.t1) + ",t_t1_min:" + str(self.t_t1_min) + ",t_t1_max:" + str(self.t_t1_max)+ ",close:" + str(tmpK.close)+ ",avg1:" + str(tmpK.avg1)            
#        logging.debug (tmplog)# + ",upM2Num:" + str(self.upM2Num))
        return 0
    
    def checkDown(self,tmpK,count):
        lastK = self.deqK[count - 2]

        if self.contindown >= 2:
            logging.error("checkDown:contindown="+str(self.contindown)+",low="+str(tmpK.low)+",TMaxdown="+str(self.TMaxdown)+",TMindown="+str(self.TMindown)+",c="+str(self.c)+",d="+str(self.d))
            if tmpK.low < self.TMindown:
                return -1
            else:
                self.resetdown()
                logging.error("checkDown:resetup=low-TMindown")
    
#        tmplog= self.symbol + ",self.down,m3m:" + str(m3m) + ",m3_m_max:" + str(self.m3_m_max)+ ",m3m2:" + str(m3m2) + ",m3_m2_max:" + str(self.m3_m2_max)+ ",m_m1_max:" + str(self.m_m1_max) + ",m:" + str(self.m)+ ",m1:" + str(self.m1)+ ",m2:" + str(self.m2) + ",m3:" + str(self.m3) +",t3:" + str(self.t3) + ",m2_m3:" + str(self.m2_m3)+ ",t:" + str(self.t) + ",t1:" + str(self.t1) + ",t_t1_min:" + str(self.t_t1_min) + ",t_t1_max:" + str(self.t_t1_max)+ ",close:" + str(tmpK.close)+ ",avg1:" + str(tmpK.avg1)            
#        logging.debug (tmplog)# + ",upM2Num:" + str(self.upM2Num))
        return 0
    
    def add(self, tmpK):
        self.deqK.append(tmpK)
        count = len(self.deqK)
        if(count < 4):
            return 0
        
        if self.longshort >= 0 and self.checkUp(tmpK,count) > 0 :
            return 1
        if self.longshort <= 0 and self.checkDown(tmpK,count) < 0 :
            return -1
        

##        if(self.t > 0 and tmpK.time + self.cycle - self.t > self.reset_t):
##            print "add,reset",tmpK.time,self.cycle,self.t,self.reset_t
##            self.reset()

        return 0
        
        
            
        
