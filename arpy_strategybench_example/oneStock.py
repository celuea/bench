# -*- coding: cp936 -*-
#from break2UpDown import break2UpDown
import ConfigParser
import logging
import PLInfo
import NetPLInfo
import ExeInfo
from candle import candle
from candleMgr import candleMgr
from break2UpDown import break2UpDown
#from avg_line
import avg_line
#import MySQLdb
'''
# "application" code
logging.debug("debug message")
logging.info("info message")
logging.warn("warn message")
logger.error("error message")

exeInfo.txt:
symbol,nLastShare, fLastPx, nH, nM, nS,iEnterOrdTimes,m_dMaxChangePx,m_dMinChangePx,
m_dOpenPrice, m_dDayHigh, m_dDayLow, m_iAllTicks/Minutes,m_dAllVolume/Minutes,m_iEnterWay,m_dSendSpread,m_chQuoteDate
self.plInfo.txt:
dPLTotal, GetTotalMktPL(), GetTotalNetPL(),m_dMaxTotalPL, m_dMinTotalPL, m_iTotalExeVolume, m_chQuoteDate
'''

SIDE_BUY = '1'
SIDE_SELL = '2'

SIGNAL_COVER = 'Y'

quoteOk = 0
selected = 0

QuoteDate = ""
##dateFile = open('curdate.tmp','r')
##QuoteDate = dateFile.read()
##print QuoteDate
##dateFile.close()
#print "111"

    
class oneStock(object):
    QuoteDate = QuoteDate
    configFl = ConfigParser.RawConfigParser()
    configFl.read(break2UpDown.confname)#"oneStock.ini")
    useStocks =  {}
    #print "222"
    
    IFopenTime = configFl.getint("tradestocks", "ifopentime")
    
    stockN = configFl.getint("tradestocks", "count")
    stockNi = 0
    while stockNi < stockN:
        keyName = "stock_" + str(stockNi)
        useStocks[configFl.get("tradestocks", keyName)] = 1
        stockNi = stockNi + 1

    confOutUpDown = configFl.getfloat("oneStock", "confOutUpDown")
    cycle = configFl.getint("oneStock", "cycleK")
    baocang=configFl.getfloat("oneStock", "baocang")
    moni=configFl.getint("oneStock", "moni")
    cltname=configFl.get("oneStock", "cltname")
    stgyname=configFl.get("oneStock", "stgyname")
    mktname=configFl.get("oneStock", "mktname")
    localport=configFl.getint("oneStock", "localport")
    serverport=configFl.getint("oneStock", "serverport")
    confcovertime=configFl.getint("oneStock", "confcovertime")
    lastopentime = configFl.getint("oneStock", "lastopentime")
    puttime = configFl.getint("oneStock", "puttime")
    
    def __init__(self,symbol,enter,cxl, signalClient, bench):#
        self.symbol = symbol
        self.enter = enter
        self.cxl = cxl
        self.signalClient = signalClient
        self.bench = bench
        
        #config
        self.multip = 1
        
        
##        config = ConfigParser.RawConfigParser()
##        config.read('stgyPanTupo.ini')
        #self.confFirstTkTime = config.getint("oneStock", "confFirstTkTime")
        #self.confBuyTime = config.getint("oneStock", "confBuyTime")
        #self.confBuyVib = config.getfloat("oneStock", "confBuyVib")
##        self.ifcode = config.get("oneStock", "ifCode")
##        self.cuCode = config.get("oneStock", "cuCode")
        
##        try:
##            self.multip = config.getint("oneStock", symbol[0])
##        except:
##            try:
##                self.multip = config.getint("oneStock", symbol[1:3])
##            except:
##                self.multip = 1

        self.kMgr = candleMgr(oneStock.cycle)  
        self.checkOpen = break2UpDown(self.symbol,oneStock.cycle)
##        self.checkOpen.cycle = oneStock.cycle
##        self.checkOpen.a = oneStock.a
##        self.checkOpen.b = oneStock.b
##        self.checkOpen.c = oneStock.c
##        self.checkOpen.d = oneStock.d
##        self.checkOpen.e = oneStock.e
##        self.checkOpen.f = oneStock.f
##        self.checkOpen.longshort = oneStock.longshort
##        #self.checkOpen.minPxSpan = config.getfloat("oneStock", "minPxSpan")
##        self.checkOpen.m_m1_max = config.getfloat("oneStock", "m_m1_max")
##        #self.checkOpen.cur_t = config.getint("oneStock", "cur_t")
##        self.checkOpen.m3_m_max =config.getfloat("oneStock", "M30M12PerBig")
##        self.checkOpen.m3_m2_max =config.getfloat("oneStock", "M32M12PerBig")
##        self.checkOpen.m2_m3 =config.getfloat("oneStock", "m2_m3")
##        self.checkOpen.t_t1_min =config.getint("oneStock", "t_t1_min")
##        self.checkOpen.t_t1_max =config.getint("oneStock", "t_t1_max")
##        self.checkOpen.tVib = config.getfloat("oneStock", "tVib")
##        self.checkOpen.reset_t = config.getint("oneStock", "resetT")

        
        #self.confTickStock = config.getint("oneStock", "confTickStock")
        #self.confOpenTime = config.getint("oneStock", "confOpenTime")
        self.lastdate=""
        self.reset()
        self.avg_line5 = avg_line.avg_line(5)
        self.avg_line10 = avg_line.avg_line(10)
        self.avg_line20 = avg_line.avg_line(20)
        self.avg_line60 = avg_line.avg_line(60)
        #self.entertime=0
        #print "oneStock init Ok"
    def reset(self):
        self.Levshare = 0
        self.trading = 1
        self.position = 0;
        self.openshare = 0;
        self.entershare = 0;#between enter and order accept
        self.enterSigPx = 0.0
        self.enterBigNum = 0
        self.stopPx = 0.0
        self.dealMaxPx = 0.0
        self.dealMinPx = 0.0
        self.enterPx = 0.0
        self.outTimes = 0
        
        self.tickN = 0
        self.dayHighPx = 0.0
        self.dayLowPx = 99999.0
        self.secSat = 0
        
        self.forceStop = 0
        self.enterTime = 0
        self.lastPx = 0.0
        self.lastTime = 0
        self.lastTotalShare = 0
        self.lastOrdId = ""
        #oneStock.confOpenTime = 33000#33000
        #oneStock.confCoverTime = 53100#54000
        oneStock.bitPx = 0.0
        self.afterTime = 0
        self.signalDirect = 0

        #self.checkOpen.resetup()
        #self.checkOpen.resetdown()        
        
#    def getQuoteDate():
#        return QuoteDate
    def onEnterSignal(self, signal,price,stop_price,share):
        self.signalDirect = signal
        self.enterSigPx = price
        self.enterBigNum = 0
        self.stopPx = stop_price
        #self.procEnterOrd(32400, price, share)
        
    def onQuote(self, time, price, share):
##        if(self.symbol == "BEGIN"):
##            self.plInfo.Clear()
##            self.plInfo.SetQuoteDate(self.QuoteDate)
#            for (d,oneStk) in self.bench.stockDict.items():
#                oneStk.Clear()
##        totalShare = share
##        share = share - self.lastTotalShare
##        if(share < 1):
##            return
##        self.lastTotalShare = totalShare
        
        global quoteOk,selected
        if(not quoteOk):
            print "quote receve: ",self.symbol,time,price, share
            #print(self.checkOpen.minPxSpan , ":minPxSpan")
            print(self.checkOpen.a , ":a")
            #print(self.checkOpen.cur_t , ":cur_t")
            print(self.checkOpen.b , ":b")
            print(self.checkOpen.c , ":c")
            print(self.checkOpen.d , ":d")
            print(self.checkOpen.e , ":e")
            print(self.checkOpen.f , ":f")
            print(self.checkOpen.longshort , ":longshort")
            #print(self.confTickStock , ":confTickStock")
            print(oneStock.confcovertime,":confcovertime")
            print(oneStock.lastopentime,":lastopentime")
            print(oneStock.puttime,":puttime")
            print(oneStock.baocang,":baocang")
            print(oneStock.baocang,":moni")
            print(oneStock.baocang,":confOutUpDown")
            quoteOk = 1
        
        tmpK = candle()
        
        #time=tmpK.time.split('')
        #print time
        if oneStock.moni > 0:
            if len(time.split(',')) < 2:
                print time,"less 2"
                return

            if cmp(time.split(',')[0].split(' ')[0],self.lastdate) != 0:
                print time.split(',')[0].split(' ')[0],self.lastdate        
                self.reset()
                
                self.plInfo = PLInfo.PLInfo()
                self.plInfo.SetRecordPLinfo(1)
                self.plInfo.SetBurnedMoney(self.baocang)
                
                self.oneNetPL = NetPLInfo.NetPLInfo()
                self.oneNetPL.setSymbol(self.symbol)
                
                self.exeInfo = ExeInfo.ExeInfo()
                self.exeInfo.SetRecordExeInfo(1)
                self.plInfo.SetQuoteDate(time.split(',')[0].split(' ')[0])
                self.oneNetPL.SetQuoteDate(time.split(',')[0].split(' ')[0])
                self.exeInfo.SetQuoteDate(time.split(',')[0].split(' ')[0])
                
            self.lastdate=time.split(',')[0].split(' ')[0]
            tmpK.open = float(time.split(',')[1])
            tmpK.close = float(time.split(',')[4])
            tmpK.high = float(time.split(',')[2])
            tmpK.low = float(time.split(',')[3])
            tmpK.share = float(time.split(',')[5])
            time=time.split(',')[0].split(' ')[1].split(':')
            timeint=int(time[0])*3600 + int(time[1])*60
            #print time,"-",timeint
            tmpK.time=timeint
        else:
            self.reset()
            
            self.plInfo = PLInfo.PLInfo()
            self.plInfo.SetRecordPLinfo(1)
            self.plInfo.SetBurnedMoney(self.baocang)
            
            self.oneNetPL = NetPLInfo.NetPLInfo()
            self.oneNetPL.setSymbol(self.symbol)
            
            self.exeInfo = ExeInfo.ExeInfo()
            self.exeInfo.SetRecordExeInfo(1)
            self.plInfo.SetQuoteDate("20140101")
            self.oneNetPL.SetQuoteDate("20140101")
            self.exeInfo.SetQuoteDate("20140101")

            time=str(time)
            #print time,time.split(':')[0],time.split(':')[1],time.split(':')[2],"==="
            timeint=int(time.split(':')[0])*3600 + int(time.split(':')[1])*60 + int(time.split(':')[2])
            print time,timeint,"---"
            #k
            #self.kMgr = candleMgr(self.cycle)
            self.kMgr.add(timeint, price, share)
            kCount = self.kMgr.size()
            if(self.kMgr.sizeChg() and kCount > 1):
                tmpK = self.kMgr.deq[kCount - 2]
                print tmpK.time,tmpK.time/3600,tmpK.time%3600/60,tmpK.time%60,tmpK.open,tmpK.high,tmpK.low,tmpK.close

        self.lastTime = timeint
        
        self.avg_line5.add(tmpK.close)
        self.avg_line10.add(tmpK.close)
        self.avg_line20.add(tmpK.close)
        self.avg_line60.add(tmpK.close)
        tmpK.avg5=round(self.avg_line5.get_avg(),4)
        tmpK.avg10=round(self.avg_line10.get_avg(),4)
        tmpK.avg20=round(self.avg_line20.get_avg(),4)
        tmpK.avg60=round(self.avg_line60.get_avg(),4)
        self.exeInfo.QuoteUpdateExeInfo(timeint, price, share)
        self.plInfo.QuoteUpdateMktPL(timeint, self.symbol,self.multip, self.position, self.enterPx, price)
        #if(self.symbol == self.confTickStock):
        #    logging.info(self.symbol + ":onQuote:" + str(time) + "," + str(price) + "," + str(share))
        
        self.lastPx = price
        self.procOutPos(price,tmpK)
        self.procEnterOrd(timeint, price, share,tmpK)
                   
    ##############         proc      #########################
    def procEnterOrd(self,time, price, share,tmpK):
        price = float(price)
        share = int(share)
        #print "--procEnterOrd--",time,oneStock.confcovertime
        if(self.plInfo.GetBurnedMoneyFlag() or time > oneStock.confcovertime ):#
            self.forceStop = 1
            return

        #print "procEnterOrd",time
        if(self.openshare != 0 or self.position != 0 or self.entershare != 0 or time > oneStock.lastopentime):
            return
        #print "procEnterOrd---",time
##        if self.symbol[0] == 'I' and self.symbol[1] == 'F':
##            if time > oneStock.IFopenTime:
##                self.tickN = self.tickN + 1
##                
##        else:
##            if time > oneStock.openTime:
##                self.tickN = self.tickN + 1
##                
##        if self.tickN > 0 and self.tickN <= 100:
##            if price > self.dayHighPx :
##                self.dayHighPx = price
##            if price < self.dayLowPx:
##                self.dayLowPx = price
##        if self.secSat == 0 and self.tickN > 100:
##            #print "tick > 100"
##            if price >= self.dayHighPx :
##                self.dayHighPx = price
##                self.secSat = 1
##            if price <= self.dayLowPx:
##                self.dayLowPx = price
##                self.secSat = 1

        
        #tmpK.chicang = time.split(',')[6]
        #print "now liuxw okkkkkkkk!"
        #print time#,tmpK
        ret = self.checkOpen.add(tmpK)
        if(ret > 1):
            print "now liuxw okkkkkkkk!>0:",ret
            self.enterOrder(self.symbol, SIDE_BUY, tmpK.avg10 + 10, 1)
            
        elif ret < -1:
            print "now liuxw okkkkkkkk!<0:",ret
            self.enterOrder(self.symbol, SIDE_SELL, tmpK.avg10 - 10, 1)  
        elif(ret > 0):
            print "now liuxw okkkkkkkk!>0:",ret
            self.enterOrder(self.symbol, SIDE_BUY, tmpK.high + 5, 1)
            
        elif ret < 0:
            print "now liuxw okkkkkkkk!<0:",ret
            self.enterOrder(self.symbol, SIDE_SELL, tmpK.low - 5, 1)        
        
               
    def procOutPos(self,price,tmpK): 
        if(self.position == 0):
            if(self.openshare == 0):
                pass
            elif(self.openshare > 0):
                if self.lastTime > self.enterTime + oneStock.puttime:
                    self.cxlOrder(self.symbol, self.lastOrdId)
            elif(self.openshare < 0):
                if self.lastTime > self.enterTime + oneStock.puttime:
                    self.cxlOrder(self.symbol, self.lastOrdId)
            
        elif(self.position > 0):#long pos               
            if(self.checkOut(self.lastTime, price,tmpK) == 1):
                self.outTimes = self.outTimes + 1
                if(self.outTimes > 0):
                    self.forceStop = 1
                print "test out ......"
            if(self.forceStop):
                if(self.lastTime > self.enterTime + 20 and self.openshare != 0):
                    logging.debug("date:" + QuoteDate + "," + self.symbol + ",procOutPos;long;" + "self.lastTime:" + str(self.lastTime)
                                  + "self.enterTime:" + str(self.enterTime)
                                  + "self.openshare:" + str(self.openshare)
                                  + "self.lastOrdId:" + self.lastOrdId )
                    self.cxlOrder(self.symbol, self.lastOrdId)
                print "test out ......---",self.openshare,self.entershare
                if(self.openshare == 0 and self.entershare == 0):
                    self.enterOrder(self.symbol, SIDE_SELL, self.dealMaxPx - self.confOutUpDown, 1)
            
        elif(self.position < 0):#short pos
            if(self.checkOut(self.lastTime, price,tmpK) == 1):
                self.outTimes = self.outTimes + 1
                if(self.outTimes > 0):
                    self.forceStop = 1
            if(self.forceStop):
                if(self.lastTime > self.enterTime + 20 and self.openshare != 0):
                    self.cxlOrder(self.symbol,self.lastOrdId)
                if(self.openshare == 0 and self.entershare == 0):
                    self.enterOrder(self.symbol, SIDE_BUY, self.dealMinPx + self.confOutUpDown, 1)

                    
    def checkOut(self, secNow,  price,tmpK):
        if oneStock.moni <= 0:
            if(price >  self.dealMaxPx):
                self.dealMaxPx = tmpK.high
            if(price  < self.dealMinPx):
                self.dealMinPx = tmpK.low
            
        if(self.position > 0):
            #
            print "checkOut long:",self.dealMaxPx,tmpK.low,price
            lossPer = self.dealMaxPx - price
            #不同出仓方法的参数最后都可以转化成止损价
            if(lossPer > self.confOutUpDown #or tmpK.low < self.checkOpen.m3
               ):#盈利多少百分点后回调多少百分点出仓
                return 1
        elif(self.position < 0):
            print "checkOut short:",self.dealMinPx,tmpK.high,price
            lossPer = price - self.dealMinPx
            #不同出仓方法的参数最后都可以转化成止损价
            if(lossPer > self.confOutUpDown  #or tmpK.high > self.checkOpen.m3
               ):
                return 1


        if oneStock.moni > 0:
            if(tmpK.high >  self.dealMaxPx):
                self.dealMaxPx = tmpK.high
            if(tmpK.low  < self.dealMinPx):
                self.dealMinPx = tmpK.low
                
        if secNow > 53700:
            return 1  
    ######################enter event #########################
    def signalClient(self, symbol, signal):
        self.signalClient(symbol, signal)
        
    def enterOrder(self, symbol,side, price, share):
        self.entershare = share
        self.enterTime=self.lastTime
        #print "enterOrder"
        print "enterOrder:",symbol,self.lastdate,self.lastTime,self.lastTime/3600,self.lastTime%3600/60,side, price, share
        if oneStock.moni > 0:
            self.onOrderExe("testxx",side, "testx", price, share)
        else:
            self.enter(symbol, side,  price, share)
    def cxlOrder(self, symbol, orderId):
        #print "cxl"
        print "enterOrder:",symbol,self.lastdate,orderId
        self.cxl(symbol, orderId)

    #######################on event ###########################    
    def onOrderAccept(self, clOrderId, orderId,side,lastShare):
        if(self.trading == 0):
            return
        logging.error("date:" + self.lastdate + "," + self.symbol + ",onOrderAccept;" + ",time:" + str(self.lastTime) + ",openshare:" + str(self.openshare) + ",position:" + str(self.position)
                      + ",clOrderId:" + clOrderId + "orderId:" + orderId +  ",side:" + str(side) + ",lastShare:" + str(lastShare))
        
        if(side == SIDE_BUY):
            self.openshare = self.openshare + lastShare
        elif(side == SIDE_SELL):
            self.openshare = self.openshare - lastShare
            
        self.lastOrdId = orderId
        
        self.entershare = 0

    def onOrderReject(self, clOrderId,orderId,side,lastShare):
        if(self.trading == 0):
            return
        logging.error("date:" + self.lastdate + "," + self.symbol  + ",onOrderReject;" + ",time:" + str(self.lastTime) + ",openshare:" + str(self.openshare) + ",position:" + str(self.position)
                      +  ",clOrderId:" + clOrderId + "orderId:" + orderId +  ",side:" + str(side) + ",lastShare:" + str(lastShare))
        
        if(side == SIDE_BUY):
            if(self.openshare != 0):
                self.openshare = self.openshare - lastShare
        elif(side == SIDE_SELL):
            if(self.openshare != 0):
                self.openshare = self.openshare + lastShare
            
        self.lastOrdId = ""
        
        self.entershare = 0

    def onCxlAccept(self, orderId,side):
        if(self.trading == 0):
            return
        logging.error("date:" + self.lastdate + "," + self.symbol  + ",onCxlAccept;" + ",time:" + str(self.lastTime) + ",openshare:" + str(self.openshare) + ",position:" + str(self.position)
                      + ",orderId:" + orderId +  ",side:" + int(side))

    def onCxlReject(self, orderId,side):
        if(self.trading == 0):
            return
        if(self.openshare != 0):
            logging.error("date:" + self.lastdate + "," + self.symbol  + ",onCxlReject;" + ",time:" + str(self.lastTime) + ",openshare:" + str(self.openshare) + ",position:" + str(self.position)
                          + ",orderId:" + orderId +  ",side:" + str(side) + ",openshare:" + str(self.openshare))
            self.openshare = 0
        
    def onCxlExe(self, orderId,side,lastShare): #lxw server need todo
        if(self.trading == 0):
            return
        logging.error("date:" + self.lastdate + "," + self.symbol  + ",onCxlExe;" + ",openshare:" + str(self.openshare) + ",position:" + str(self.position)
                      + ",orderId:" + orderId +  ",side:" + str(side) + ",lastShare:" + str(lastShare))
        if(side == SIDE_BUY):
            self.openshare = self.openshare - lastShare
        elif(side == SIDE_SELL):
            self.openshare = self.openshare + lastShare

    def onOrderExe(self, orderId,side, exeId, price, lastShare):
        if(self.trading == 0):
            return        
        logging.error("date:" + self.lastdate + "," + self.symbol  + ",onOrderExe;" + ",time:" + str(self.lastTime) + ",openshare:" + str(self.openshare) + ",position:" + str(self.position)
                      + ",lastOrdId:" + self.lastOrdId + ",orderId:" + orderId + ",exeId:" + exeId
                      +  ",side:" + str(side) + ",lastShare:" + str(lastShare)
                      + ",EnterNum:" + str(self.oneNetPL.GetEnterNum()) + ",OneNetPL:" + str(self.oneNetPL.GetOneNetPL()))

        print "onOrderExe,+++++-------",self.position,self.lastTime,self.multip, side, lastShare, price
        self.oneNetPL.CalcOneStockPL(self.position,self.lastTime,self.multip, side, lastShare, price)
        print "onOrderExe,+++",self.symbol, self.lastTime, self.oneNetPL.GetEnterNum(), side, lastShare, price
        self.exeInfo.ExeUpdateExeInfo(self.symbol, self.lastTime, self.oneNetPL.GetEnterNum(), side, lastShare, price)
        
        if(side == SIDE_BUY):
            if(len(self.lastOrdId) > 1):
                self.openshare = self.openshare - lastShare
            self.position = self.position + lastShare
        elif(side == SIDE_SELL):
            if(len(self.lastOrdId) > 1):
                self.openshare = self.openshare + lastShare
            self.position = self.position - lastShare

        self.plInfo.UpdateNetPL(self.symbol, self.oneNetPL.GetOneNetPL(), lastShare)
##        if(self.position == 0):
##            self.signalClient(self.symbol, SIGNAL_COVER)
        #reset
        
        self.outTimes = 0
        self.enterPx = price
        self.dealMaxPx = price
        self.dealMinPx = price
        self.lastOrdId = ""
        self.entershare = 0
        self.forceStop = 0
        if self.position == 0:
            if side == SIDE_BUY:
                self.checkOpen.resetdown()
            elif side == SIDE_SELL:
                self.checkOpen.resetup()
    
def main():
    pass
    #test = oneStock("111",0,0, 0, 0)
if __name__ == '__main__':
    pass
#    import oneStock
#    oneStock.main()
    
